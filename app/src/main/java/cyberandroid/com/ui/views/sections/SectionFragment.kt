package cyberandroid.com.ui.views.sections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import cyberandroid.com.databinding.FragmentSectionBinding
import cyberandroid.com.ui.viewmodels.SectionsViewModel

class SectionFragment : Fragment() {
    private var _binding: FragmentSectionBinding? = null
    private val binding get() = _binding!!

    private val sectionsViewModel: SectionsViewModel by viewModels()
    private var sectionId = 0
    private val args: SectionFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSectionBinding.inflate(inflater, container, false)
        sectionId = args.sectionId

        sectionsViewModel.loadSection(sectionId)

        sectionsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.sectionsShimmer.isVisible = it
            binding.content.isVisible = !it
        })

        sectionsViewModel.section.observe(viewLifecycleOwner, Observer { section ->
            binding.titleText.text = section.title
            binding.paragraphText.text = section.paragraph

            Picasso.get().load(section.image).into(binding.sectionImage)
        })
        return binding.root
    }
}