package cyberandroid.com.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import cyberandroid.com.R
import cyberandroid.com.data.models.sections.Section
import cyberandroid.com.databinding.ItemSectionBinding
import cyberandroid.com.ui.views.sections.SectionsFragmentDirections

class SectionsAdapter(private val sections: List<Section>) :
    RecyclerView.Adapter<SectionsAdapter.SectionsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return SectionsViewHolder(
            layoutInflater.inflate(
                R.layout.item_section, parent, false
            ), parent.context
        )
    }

    override fun onBindViewHolder(holder: SectionsViewHolder, position: Int) {
        holder.render(sections[position])

        val section: Section = sections[position]
        val binding = ItemSectionBinding.bind(holder.itemView)

        binding.textView.text = section.title
        Picasso.get().load(section.image).into(binding.imageView)
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    class SectionsViewHolder(view: View, ct: Context) : RecyclerView.ViewHolder(view) {
        private val binding = ItemSectionBinding.bind(view)
        private val context = ct

        fun render(section: Section) {
            binding.sectionCard.setOnClickListener {
                val navController: NavController = Navigation.findNavController(binding.root)

                val action =
                    SectionsFragmentDirections.actionSectionsFragmentToSectionFragment(section.id)
                navController.navigate(action)
            }
        }
    }
}