package cyberandroid.com.ui.views.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import cyberandroid.com.R
import cyberandroid.com.databinding.FragmentProductBinding
import cyberandroid.com.ui.viewmodels.ProductsViewModel

class ProductFragment : Fragment() {
    private var _binding: FragmentProductBinding? = null
    private val binding get() = _binding!!

    private val productsViewModel: ProductsViewModel by viewModels()
    private var productId = 0
    private val args: ProductFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        productId = args.productId

        productsViewModel.loadProduct(productId)

        productsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.productsShimmer.isVisible = it
            binding.content.isVisible = !it
        })

        productsViewModel.product.observe(viewLifecycleOwner, Observer { product ->
            binding.productName.text = product.name
            binding.productDescription.text = product.description
            binding.productPrice.text = getString(R.string.price_format, product.price)
            binding.productManufacturer.text = product.manufacturerName
            binding.productCategory.text = product.categoryName
            binding.productDistributor.text = product.distributorName
            binding.productSupplier.text = product.supplierName

            Picasso.get().load(product.image).into(binding.productImage)
        })
        return binding.root
    }
}