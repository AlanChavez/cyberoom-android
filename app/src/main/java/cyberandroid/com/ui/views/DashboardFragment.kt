package cyberandroid.com.ui.views

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cyberandroid.com.data.models.Dashboard
import cyberandroid.com.databinding.FragmentDashboardBinding
import cyberandroid.com.ui.views.adapters.DashboardAdapter

class DashboardFragment : Fragment() {
    private var _binding: FragmentDashboardBinding? = null
    private val binding get() = _binding!!

    private lateinit var dashboardAdapter: DashboardAdapter
    private var options: List<Dashboard> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)

        loadOptions()

        val recyclerView = binding.dashboardRecycler
        dashboardAdapter = DashboardAdapter(options)

        val layoutManager = GridLayoutManager(context, 1)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return 1 // Siempre una columna
            }
        }

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = dashboardAdapter
        recyclerView.addItemDecoration(GridSpacingItemDecoration(1, 16, true))

        return binding.root
    }

    private fun loadOptions() {
        options = listOf(
            Dashboard(
                1,
                "Sections",
                "https://www.kindpng.com/picc/m/11-116582_grafica-de-pastel-png-transparent-png.png"
            ),
            Dashboard(
                2,
                "Products",
                "https://static.vecteezy.com/system/resources/previews/002/392/527/non_2x/cartoon-illustration-of-open-empty-parcel-box-free-vector.jpg"
            ),
        )
    }
}

class GridSpacingItemDecoration(
    private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view) // posición del elemento actual
        val column = position % spanCount // columna del elemento actual

        if (includeEdge) {
            outRect.left =
                spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
            outRect.right =
                (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.top = spacing
            }
            outRect.bottom = spacing // item bottom
        } else {
            outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
            outRect.right =
                spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = spacing // item top
            }
        }
    }
}
