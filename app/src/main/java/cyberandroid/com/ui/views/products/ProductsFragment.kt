package cyberandroid.com.ui.views.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import cyberandroid.com.databinding.FragmentProductsBinding
import cyberandroid.com.ui.viewmodels.ProductsViewModel
import cyberandroid.com.ui.views.adapters.ProductsAdapter

class ProductsFragment : Fragment() {
    private var _binding: FragmentProductsBinding? = null
    private val binding get() = _binding!!
    private val productsViewModel: ProductsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductsBinding.inflate(inflater, container, false)

        productsViewModel.loadProducts()

        productsViewModel.products.observe(viewLifecycleOwner, Observer { products ->
            binding.productsRecycler.layoutManager = LinearLayoutManager(context)
            val adapter = ProductsAdapter(products)
            binding.productsRecycler.adapter = adapter

            if (products.isNotEmpty()) {
                binding.productsShimmer.visibility = View.GONE
                binding.productsRecycler.visibility = View.VISIBLE
            }
        })

        productsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.productsShimmer.isVisible = it
        })

        return binding.root
    }
}