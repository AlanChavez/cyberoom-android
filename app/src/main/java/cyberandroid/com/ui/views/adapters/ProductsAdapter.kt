package cyberandroid.com.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import cyberandroid.com.R
import cyberandroid.com.data.models.products.Product
import cyberandroid.com.databinding.ItemProductBinding
import cyberandroid.com.ui.views.products.ProductsFragmentDirections

class ProductsAdapter(private val products: List<Product>) :
    RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ProductsViewHolder(
            layoutInflater.inflate(
                R.layout.item_product, parent, false
            ), parent.context
        )
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        holder.render(products[position])

        val product: Product = products[position]
        val binding = ItemProductBinding.bind(holder.itemView)

        binding.textView.text = product.name
        Picasso.get().load(product.image).into(binding.imageView)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    class ProductsViewHolder(view: View, ct: Context) : RecyclerView.ViewHolder(view) {
        private val binding = ItemProductBinding.bind(view)
        private val context = ct

        fun render(product: Product) {
            binding.productCard.setOnClickListener {
                val navController: NavController = Navigation.findNavController(binding.root)

                val action =
                    ProductsFragmentDirections.actionProductsFragmentToProductFragment(product.id)
                navController.navigate(action)
            }
        }
    }
}