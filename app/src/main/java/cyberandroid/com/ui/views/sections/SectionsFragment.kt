package cyberandroid.com.ui.views.sections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import cyberandroid.com.databinding.FragmentSectionsBinding
import cyberandroid.com.ui.viewmodels.SectionsViewModel
import cyberandroid.com.ui.views.adapters.SectionsAdapter

class SectionsFragment : Fragment() {
    private var _binding: FragmentSectionsBinding? = null
    private val binding get() = _binding!!
    private val sectionsViewModel: SectionsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSectionsBinding.inflate(inflater, container, false)

        sectionsViewModel.loadSections()

        sectionsViewModel.sections.observe(viewLifecycleOwner, Observer { sections ->
            binding.sectionsRecycler.layoutManager = LinearLayoutManager(context)
            val adapter = SectionsAdapter(sections)
            binding.sectionsRecycler.adapter = adapter

            if (sections.isNotEmpty()) {
                binding.sectionsShimmer.visibility = View.GONE
                binding.sectionsRecycler.visibility = View.VISIBLE
            }
        })

        sectionsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.sectionsShimmer.isVisible = it
        })

        return binding.root
    }
}