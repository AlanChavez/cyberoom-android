package cyberandroid.com.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cyberandroid.com.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}