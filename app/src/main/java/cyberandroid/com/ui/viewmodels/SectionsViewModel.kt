package cyberandroid.com.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import cyberandroid.com.data.models.sections.Section
import cyberandroid.com.domain.sections.GetAllSections
import cyberandroid.com.domain.sections.GetSection

class SectionsViewModel : ViewModel() {

    val sections = MutableLiveData<List<Section>>()
    val section = MutableLiveData<Section>()
    val isLoading = MutableLiveData<Boolean>()

    var getAllSections = GetAllSections()
    var getSection = GetSection()

    fun loadSections() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllSections()

            if (!result.isNullOrEmpty()) {
                sections.postValue(result)
                isLoading.postValue(false)
            }
        }
    }

    fun loadSection(section_id: Int) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getSection.invoke(section_id)
            section.postValue(result)
            isLoading.postValue(false)
        }
    }

}
