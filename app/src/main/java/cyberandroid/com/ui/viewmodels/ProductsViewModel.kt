package cyberandroid.com.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cyberandroid.com.data.models.products.Product
import cyberandroid.com.domain.products.GetAllProducts
import cyberandroid.com.domain.products.GetProduct
import kotlinx.coroutines.launch

class ProductsViewModel : ViewModel() {
    val products = MutableLiveData<List<Product>>()
    val product = MutableLiveData<Product>()
    val isLoading = MutableLiveData<Boolean>()

    var getAllProducts = GetAllProducts()
    var getProduct = GetProduct()

    fun loadProducts() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllProducts()

            if (!result.isNullOrEmpty()) {
                products.postValue(result)
                isLoading.postValue(false)
            }
        }
    }

    fun loadProduct(product_id: Int) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getProduct.invoke(product_id)
            product.postValue(result)
            isLoading.postValue(false)
        }
    }
}