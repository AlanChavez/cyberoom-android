package cyberandroid.com.data.repositories

import cyberandroid.com.data.models.sections.Section
import cyberandroid.com.data.models.sections.SectionProvider
import cyberandroid.com.data.models.sections.SectionsProvider
import cyberandroid.com.data.network.Sections.SectionService

class SectionsRepository {
    private val api = SectionService()

    suspend fun getAllSections(): List<Section> {
        val response = api.getSections()
        SectionsProvider.sections = response
        return response
    }

    suspend fun getSection(section_id: Int): Section {
        val response = api.getSection(section_id)
        SectionProvider.section = response
        return response
    }
}