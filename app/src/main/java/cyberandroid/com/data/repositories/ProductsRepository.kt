package cyberandroid.com.data.repositories

import cyberandroid.com.data.models.products.Product
import cyberandroid.com.data.models.products.ProductProvider
import cyberandroid.com.data.models.products.ProductsProvider
import cyberandroid.com.data.network.Products.ProductService

class ProductsRepository {
    private val api = ProductService()

    suspend fun getAllProducts(): List<Product> {
        val response = api.getProducts()
        ProductsProvider.products = response
        return response
    }

    suspend fun getProduct(product_id: Int): Product {
        val response = api.getProduct(product_id)
        ProductProvider.product = response
        return response
    }
}