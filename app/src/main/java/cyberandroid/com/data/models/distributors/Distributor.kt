package cyberandroid.com.data.models.distributors

import com.google.gson.annotations.SerializedName

data class Distributor(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("image") val image: String,
    @SerializedName("created_at") val created: String,
)
