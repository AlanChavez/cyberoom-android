package cyberandroid.com.data.models.sections

import com.google.gson.annotations.SerializedName

data class Section(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("paragraph") val paragraph: String,
    @SerializedName("image") val image: String,
    @SerializedName("created_at") val created: String,
    )
