package cyberandroid.com.data.models.products

import com.google.gson.annotations.SerializedName
import cyberandroid.com.data.models.categories.Category
import cyberandroid.com.data.models.distributors.Distributor
import cyberandroid.com.data.models.manufacturers.Manufacturer
import cyberandroid.com.data.models.suppliers.Supplier

data class Product(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("image") val image: String,
    @SerializedName("price") val price: Double,
    @SerializedName("manufacturer") val manufacturerName: String?,
    @SerializedName("category") val categoryName: String?,
    @SerializedName("distributor") val distributorName: String?,
    @SerializedName("supplier") val supplierName: String?,
    @SerializedName("created_at") val created: String
)
