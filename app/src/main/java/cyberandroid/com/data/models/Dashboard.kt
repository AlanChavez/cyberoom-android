package cyberandroid.com.data.models

data class Dashboard(
    var id: Int,
    var name: String,
    var image: String,
)
