package cyberandroid.com.data.network.Products

import cyberandroid.com.data.models.products.Product
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductApiClient {
    @GET("products")
    suspend fun getAllProducts(): Response<List<Product>>

    @GET("products/{id}")
    suspend fun getProducts(@Path("id") id: Int): Response<Product>
}