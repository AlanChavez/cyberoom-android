package cyberandroid.com.data.network.Sections

import cyberandroid.com.data.models.sections.Section
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface SectionApiClient {
    @GET("sections")
    suspend fun getAllSections(): Response<List<Section>>

    @GET("sections/{id}")
    suspend fun getSection(@Path("id") id: Int): Response<Section>
}