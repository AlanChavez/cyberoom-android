package cyberandroid.com.data.network.Products

import cyberandroid.com.core.RetrofitHelper
import cyberandroid.com.data.models.products.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProductService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getProducts(): List<Product> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                ProductApiClient::class.java
            ).getAllProducts()
            response.body() ?: emptyList()
        }
    }

    suspend fun getProduct(product_id: Int): Product {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                ProductApiClient::class.java
            ).getProducts(product_id)
            response.body()!!
        }
    }
}