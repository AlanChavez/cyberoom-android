package cyberandroid.com.data.network.Sections

import cyberandroid.com.core.RetrofitHelper
import cyberandroid.com.data.models.sections.Section
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SectionService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getSections(): List<Section> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                SectionApiClient::class.java
            ).getAllSections()
            response.body() ?: emptyList()
        }
    }

    suspend fun getSection(section_id: Int): Section {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                SectionApiClient::class.java
            ).getSection(section_id)
            response.body()!!
        }
    }
}