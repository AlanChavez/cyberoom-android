package cyberandroid.com.domain.products

import cyberandroid.com.data.repositories.ProductsRepository

class GetProduct {
    private val repository = ProductsRepository()
    suspend operator fun invoke(product_id: Int) = repository.getProduct(product_id)
}