package cyberandroid.com.domain.products

import cyberandroid.com.data.repositories.ProductsRepository

class GetAllProducts {
    private val repository = ProductsRepository()
    suspend operator fun invoke() = repository.getAllProducts()
}