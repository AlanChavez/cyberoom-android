package cyberandroid.com.domain.sections

import cyberandroid.com.data.repositories.SectionsRepository

class GetSection {
    private val repository = SectionsRepository()
    suspend operator fun invoke(section_id: Int) = repository.getSection(section_id)
}