package cyberandroid.com.domain.sections

import cyberandroid.com.data.repositories.SectionsRepository

class GetAllSections {
    private val repository = SectionsRepository()
    suspend operator fun invoke() = repository.getAllSections()
}